# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/boris/Desktop/izg/proj/src/student/application.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/application.c.o"
  "/home/boris/Desktop/izg/proj/src/student/bunny.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/bunny.c.o"
  "/home/boris/Desktop/izg/proj/src/student/callbackAndData.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/callbackAndData.c.o"
  "/home/boris/Desktop/izg/proj/src/student/camera.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/camera.c.o"
  "/home/boris/Desktop/izg/proj/src/student/cpu.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/cpu.c.o"
  "/home/boris/Desktop/izg/proj/src/student/emptyMethod.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/emptyMethod.c.o"
  "/home/boris/Desktop/izg/proj/src/student/globals.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/globals.c.o"
  "/home/boris/Desktop/izg/proj/src/student/gpu.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/gpu.c.o"
  "/home/boris/Desktop/izg/proj/src/student/linearAlgebra.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/linearAlgebra.c.o"
  "/home/boris/Desktop/izg/proj/src/student/main.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/main.c.o"
  "/home/boris/Desktop/izg/proj/src/student/method.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/method.c.o"
  "/home/boris/Desktop/izg/proj/src/student/parseArguments.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/parseArguments.c.o"
  "/home/boris/Desktop/izg/proj/src/student/phongMethod.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/phongMethod.c.o"
  "/home/boris/Desktop/izg/proj/src/student/pointBoxMethod.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/pointBoxMethod.c.o"
  "/home/boris/Desktop/izg/proj/src/student/pointCircleMethod.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/pointCircleMethod.c.o"
  "/home/boris/Desktop/izg/proj/src/student/pointMethod.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/pointMethod.c.o"
  "/home/boris/Desktop/izg/proj/src/student/pointSquareMethod.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/pointSquareMethod.c.o"
  "/home/boris/Desktop/izg/proj/src/student/triangle3DMethod.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/triangle3DMethod.c.o"
  "/home/boris/Desktop/izg/proj/src/student/triangleMethod.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/triangleMethod.c.o"
  "/home/boris/Desktop/izg/proj/src/student/window.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/window.c.o"
  "/home/boris/Desktop/izg/proj/src/tests/groundTruth.c" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/tests/groundTruth.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../"
  "../stack/src"
  "stack"
  "../stack"
  "../errorCodes/src"
  "errorCodes"
  "../errorCodes"
  "../queue/src"
  "queue"
  "../queue"
  "../vector/src"
  "vector"
  "../vector"
  "/home/boris/Desktop/izg/proj/install/include/SDL2"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/boris/Desktop/izg/proj/src/student/drawPoints.cpp" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/drawPoints.cpp.o"
  "/home/boris/Desktop/izg/proj/src/student/drawTriangles.cpp" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/student/drawTriangles.cpp.o"
  "/home/boris/Desktop/izg/proj/src/tests/conformanceTests.cpp" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/tests/conformanceTests.cpp.o"
  "/home/boris/Desktop/izg/proj/src/tests/performanceTest.cpp" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/tests/performanceTest.cpp.o"
  "/home/boris/Desktop/izg/proj/src/tests/takeScreenShot.cpp" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/tests/takeScreenShot.cpp.o"
  "/home/boris/Desktop/izg/proj/src/tests/tests.cpp" "/home/boris/Desktop/izg/proj/src/build/CMakeFiles/izgProject.dir/tests/tests.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../"
  "../stack/src"
  "stack"
  "../stack"
  "../errorCodes/src"
  "errorCodes"
  "../errorCodes"
  "../queue/src"
  "queue"
  "../queue"
  "../vector/src"
  "vector"
  "../vector"
  "/home/boris/Desktop/izg/proj/install/include/SDL2"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/boris/Desktop/izg/proj/src/build/stack/CMakeFiles/stack.dir/DependInfo.cmake"
  "/home/boris/Desktop/izg/proj/src/build/queue/CMakeFiles/queue.dir/DependInfo.cmake"
  "/home/boris/Desktop/izg/proj/src/build/vector/CMakeFiles/vector.dir/DependInfo.cmake"
  "/home/boris/Desktop/izg/proj/src/build/errorCodes/CMakeFiles/errorCodes.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

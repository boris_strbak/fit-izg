# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/boris/Desktop/izg/proj/src/stack/src/stack/stack.c" "/home/boris/Desktop/izg/proj/src/build/stack/CMakeFiles/stack.dir/src/stack/stack.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "stack"
  "../stack"
  "../stack/src"
  "../errorCodes/src"
  "errorCodes"
  "../errorCodes"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/boris/Desktop/izg/proj/src/build/errorCodes/CMakeFiles/errorCodes.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
